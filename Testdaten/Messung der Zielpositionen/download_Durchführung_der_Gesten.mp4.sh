#!/bin/bash

# Link: https://drive.google.com/file/d/1jFyrHsJJDY_zgrn6hLpjdDA1yS6WOG0a/view?usp=sharing

FILE_ID="1jFyrHsJJDY_zgrn6hLpjdDA1yS6WOG0a"
FILE_NAME="Durchführung der Gesten.mp4"
wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate "https://docs.google.com/uc?export=download&id=$FILE_ID" -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=$FILE_ID" -O "$FILE_NAME" && rm -rf /tmp/cookies.txt
