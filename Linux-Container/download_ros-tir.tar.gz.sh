#!/bin/bash

# Link: https://drive.google.com/file/d/12iaaY6u62DYRTx4tipUpbsukeHCAC6vK/view?usp=sharing

FILE_ID="12iaaY6u62DYRTx4tipUpbsukeHCAC6vK"
FILE_NAME="ros-tir.tar.gz"
wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate "https://docs.google.com/uc?export=download&id=$FILE_ID" -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=$FILE_ID" -O "$FILE_NAME" && rm -rf /tmp/cookies.txt
